#pragma once

enum HttpHeaderType
{
	Host,
	Accept,
	ContentLength,
	ContentType,
	TransferEncoding,
	Connection,
	UserAgent,
	AcceptEncoding,

	HttpHeaderType_MAX,
};