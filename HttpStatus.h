#pragma once

#include "common/String.h"

enum HttpStatusCode
{
	OK = 200,
};

struct HttpStatus
{
	HttpStatus() {}

	HttpStatus(HttpStatusCode statusCode, String description) : statusCode(statusCode), description(description)
	{
		// do nothing
	}

	HttpStatusCode statusCode;
	String description;
};