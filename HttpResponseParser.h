#pragma once

#include "common/Array.h"

#include "Zip.h"

enum ParsingState
{
	ReadingResponse,
	ReadingHeaders,
	ReadingChunkedHeader,
	ReadingChunkedData,
	ReadingRawData,
	Complete,
};

class HttpResponseParser
{
public:
	HttpResponseParser(HttpResponseParserListener* listener);
	~HttpResponseParser();

	bool parse(Array<char> data);

private:
	bool readRawData();
	bool readHeaders();
	bool readResponse();
	bool readChunkedHeader();
	bool readChunkedData();

	HttpResponseParserListener* _listener;

	// state variables
	ParsingState _state;
	union {
		size_t _chunkedDataSize;    // _state == ReadingChunkedHeader
		size_t _chunkedDataToRead;  // _state == ReadingChunkedData
		size_t _rawDataToRead;		// _state == ReadingRawData
	};

	bool _chunked;
	size_t _numBytesToSkip;
	Zip _data;
	
	char* _storage;
	unsigned int _storageCapacity;
};