#include "Zip.h"

#include "HttpResponseParserListener.h"

#include "common/Enforce.h"
#include "common/Array.hpp"
#include "common/Buffer.hpp"
#include "common/String.h"

Zip::Zip()
{
	// do nothing
}

Zip::Zip( Array<char> prev, Array<char> cur ) : prev(prev), cur(cur)
{
	// do nothing
}

char Zip::operator[]( size_t index )
{
	enforce(index < length(), "Out of bounds error");

	if (prev.length > index) {
		return prev[index];
	}

	return cur[index - prev.length];
}

size_t Zip::indexOf( size_t pos, char c )
{
	for ( ; pos < prev.length; ++pos) {
		if (prev[pos] == c) return pos;
	}

	pos -= prev.length;

	for ( ; pos < cur.length; ++pos) {
		if (cur[pos] == c) return pos + prev.length;
	}

	return -1;
}

void Zip::copyTo( Buffer<char>& buffer )
{
	buffer.put(prev);
	buffer.put(cur);
}

void Zip::moveTo( Buffer<char>& buffer, size_t from, size_t to )
{
	enforce(from <= to, "Out of bounds error");
	enforce(to <= length(), "Out of bounds error");
	enforce(from <= to, "Invalid arguments");

	if (prev.length >= to) {
		buffer.put(prev.slice(from, to));
		prev = prev.slice(to);
	} else if (prev.length < from) {
		buffer.put(cur.slice(from - prev.length, to - prev.length));
		prev = cur.slice(to - prev.length);
		cur.length = 0;
	} else {
		buffer.put(prev.slice(from));
		buffer.put(cur.slice(0, to - prev.length));
		prev = cur.slice(to - prev.length);
		cur.length = 0;
	}
}

void Zip::commitTo( HttpResponseParserListener* listener, size_t from, size_t to )
{
	enforce(from <= to, "Out of bounds error");
	enforce(to <= length(), "Out of bounds error");
	enforce(from <= to, "Invalid arguments");

	if (prev.length >= to) {
		listener->onContents(prev.slice(from, to));
		prev = prev.slice(to);
	} else if (prev.length < from) {
		listener->onContents(cur.slice(from - prev.length, to - prev.length));
		prev = cur.slice(to - prev.length);
		cur.length = 0;
	} else {
		listener->onContents(prev.slice(from));
		listener->onContents(cur.slice(0, to - prev.length));
		prev = cur.slice(to - prev.length);
		cur.length = 0;
	}
}

Zip Zip::extractZip( size_t from, size_t to )
{
	enforce0(from <= to);

	if (prev.length >= to) {
		return Zip(prev.slice(from, to), Array<char>());
	}

	if (prev.length < from) {
		return Zip(cur.slice(from - prev.length, to - prev.length), Array<char>());
	}

	return Zip(prev.slice(from), cur.slice(0, to - prev.length));
}

Zip Zip::extractZip( size_t from )
{
	if (cur.length == 0) {
		return Zip(prev.slice(from), Array<char>());
	}

	if (prev.length <= from) {
		return Zip(cur.slice(from - prev.length), Array<char>());
	}

	return Zip(prev.slice(from), cur.slice(0));
}

void Zip::skipBefore( size_t pos )
{
	enforce0(length() >= pos);
	if (pos < prev.length) {
		prev = prev.slice(pos);
	} else {
		prev = cur.slice(pos - prev.length);
		cur.length = 0;
	}
}

size_t Zip::length()
{
	return prev.length + cur.length;
}

String Zip::toString()
{
	size_t length = this->length();
	char* requestString = new char[length + 1];
	memcpy(requestString, prev.ptr, prev.length);
	memcpy(requestString + prev.length, cur.ptr, cur.length);

	requestString[length] = 0;

	return String(length, requestString);
}

bool Zip::operator==( String string )
{
	size_t length = this->length();
	if (length != string.length) {
		return false;
	}

	if (memcmp(prev.ptr, string.ptr, prev.length) != 0) {
		return false;
	}

	if (memcmp(cur.ptr, string.ptr + prev.length, cur.length) != 0) {
		return false;
	}

	return true;
}

size_t Zip::toInt()
{
	size_t result = 0;
	for (size_t i = 0; i < prev.length; ++i) {
		result = result * 10 + prev[i] - '0';
	}

	for (size_t i = 0; i < cur.length; ++i) {
		result = result * 10 + cur[i] - '0';
	}

	return result;
}
