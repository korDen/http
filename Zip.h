#pragma once

#include "common/Array.h"
#include "common/Buffer.h"

#include <stdlib.h>

class String;
struct HttpResponseParserListener;

class Zip
{
public:
	Zip();

	Zip(Array<char> prev, Array<char> cur);

	char operator[](size_t index);

	size_t indexOf(size_t pos, char c);

	void copyTo(Buffer<char>& buffer);
	void moveTo(Buffer<char>& buffer, size_t from, size_t to);

	void commitTo(HttpResponseParserListener* listener, size_t from, size_t to);

	Zip extractZip(size_t from, size_t to);
	Zip extractZip(size_t from);

	size_t toInt();

	bool operator==(String string);

	String toString();

	void skipBefore(size_t pos);

	size_t length();

	Array<char> prev;
	Array<char> cur;
};