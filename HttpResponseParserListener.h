#pragma once

#include "HttpStatus.h"

class Zip;

template<typename T> struct Array;

struct HttpResponseParserListener
{
	virtual void onStatus(HttpStatusCode statusCode, Zip description) = 0;
	virtual void onHeader(Zip key, Zip value) = 0;
	virtual void onContentsSize(size_t size) = 0;
	virtual void onContents(Array<char> data) = 0;
	virtual void onContentsEnd() = 0;
};