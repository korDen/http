#pragma once

#include "HttpParam.h"
#include "HttpHeader.h"
#include "HttpRequestType.h"
#include "HttpRequestParserListener.h"

#include "common/Buffer.h"

#include "Zip.h"

class HttpRequest
{
public:
	HttpRequest(String host, String path, HttpRequestType requestType);

	void addParam(HttpParam param); // &k=v
	void addParam(String key, String value); // &k=v

	void addHeader(const HttpHeader& header);
	void setHeader(const HttpHeader& header);

	Array<HttpParam> params();
	Array<HttpHeader> headers();

	HttpRequestType requestType();
	void requestType(HttpRequestType requestType);

	String host();
	void host(String host);
	
	String path();
	void path(String path);

	Array<char> compose( Buffer<char>& buffer );

private:
	String _path;

	Buffer<HttpParam> _params;
	Buffer<HttpHeader> _headers;

	HttpRequestType _requestType;
};