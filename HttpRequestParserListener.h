#pragma once

#include "HttpRequestType.h"

struct HttpHeader;

class Zip;

enum HttpRequestMethod
{
	OPTIONS,
	GET,
	HEAD,
	POST,

	//	PUT,
	//	DELETE,
	//	TRACE,
	//	CONNECT,

	UNKNOWN,
};

struct HttpRequestParserListener
{
	virtual void onInit(HttpRequestMethod method, Zip requestString) = 0;
	virtual void onContentsSize(size_t size) = 0;
	virtual void onHeader(Zip key, Zip value) = 0;
};