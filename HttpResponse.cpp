#include "HttpResponse.h"
#include "common/Buffer.hpp"

HttpResponse::HttpResponse()
{
	// do nothing
}

HttpResponse::~HttpResponse()
{
	for (size_t i = 0, length = _headers.length(); i < length; ++i) {
		delete[] _headers[i].key.ptr;
		delete[] _headers[i].value.ptr;
	}

	delete[] _httpStatus.description.ptr;
}

const HttpStatus& HttpResponse::httpStatus() const
{
	return _httpStatus;
}

Array<HttpHeader> HttpResponse::headers() const
{
	return _headers.data();
}

Array<char> HttpResponse::contents() const
{
	return _contents.data();
}

void HttpResponse::onStatus( HttpStatusCode statusCode, Zip description )
{
	_httpStatus = HttpStatus(statusCode, description.toString());
}

void HttpResponse::onHeader( Zip key, Zip value )
{
	_headers.put(HttpHeader(key.toString(), value.toString()));
}

void HttpResponse::onContentsSize( size_t size )
{
	_contents.reserve(_contents.length() + size);
}

void HttpResponse::onContents( Array<char> data )
{
	_contents.put(data);
}

void HttpResponse::onContentsEnd()
{
	// do nothing
}