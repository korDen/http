#pragma once

#include "HttpHeaders.h"
#include "HttpHeaderType.h"

#include "common/Array.hpp"

struct HttpHeader
{
	HttpHeader(HttpHeaderType type, String value) : key(HttpHeaders[type]), value(value)
	{
		// do nothing
	}

	HttpHeader(String key, String value) : key(key), value(value)
	{
		// do nothing
	}

	String key;
	String value;
};