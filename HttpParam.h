#pragma once

#include "common/String.h"

struct HttpParam
{
	HttpParam(String key, String value) : key(key), value(value)
	{
		// do nothing
	}

	String key;
	String value;
};