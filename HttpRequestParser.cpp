#include "HttpRequestParser.h"

#include "HttpHeader.h"
#include "HttpHeaders.h"
#include "HttpRequestParserListener.h"

#include "HttpRequestType.h"

#include "common/Memory.h"
#include "common/String.h"

HttpRequestParser::HttpRequestParser(HttpRequestParserListener* listener) : _state(ReadingRequest), _rawDataToRead(0), _listener(listener), _storage(nullptr), _storageCapacity(0)
{
	// do nothing
}

HttpRequestParser::~HttpRequestParser()
{
	free(_storage);
}

bool HttpRequestParser::parse( Array<char> data )
{
	// Now append data to existing data:
	// _data = _data + data

	if (_data.cur.length != 0) {
		// merge prev and cur into prev
		unsigned int size = _data.length();
		if (_storageCapacity < size) {
			_storage = (char*)reallocMove(_storage, size, 0);
			_storageCapacity = size;
		}

		memcpy(_storage, _data.prev.ptr, _data.prev.length);
		memcpy(_storage + _data.prev.length, _data.cur.ptr, _data.cur.length);
		_data.prev = Array<char>(size, _storage);
	}

	_data.cur = data;

	_numBytesToSkip = 0;

	bool finished = false;

	switch (_state) {
	case ReadingRequest:
		finished = readRequest();
		break;

	case ReadingHeaders:
		finished = readHeaders();
		break;

	case ReadingRawData:
		finished = readRawData();
		break;

	default:
		enforce0(false);
		break;
	}

	if (finished) {
		_state = Complete;
		return true;
	}

	return false;
}

HttpRequestMethod parseHttpRequestMethod(Zip method)
{
	if (method == "OPTIONS") {
		return OPTIONS;
	} else if (method == "GET") {
		return GET;
	} else if (method == "HEAD") {
		return HEAD;
	} else if (method == "POST") {
		return POST;
	}
	
	return UNKNOWN;
}

bool HttpRequestParser::readRequest()
{
	size_t pos = _data.indexOf(0, '\n');
	if (pos == -1) {
		return false;
	}

	size_t spacePos = _data.indexOf(0, ' ');
	Zip method = _data.extractZip(0, spacePos);
	++spacePos;

	size_t spacePos2 = _data.indexOf(spacePos, ' ');

	Zip request = _data.extractZip(spacePos, spacePos2);
	_listener->onInit(parseHttpRequestMethod(method), request);

	_data.skipBefore(pos + 1);

	_state = ReadingHeaders;

	return readHeaders();
}

bool HttpRequestParser::readHeaders()
{
	size_t begin = 0;
	while (true) {
		size_t end = _data.indexOf(begin, '\r');
		if (end == -1) {
			size_t len = _data.length();
			if (len >= begin) {
				_data.skipBefore(begin);
			} else {
				_data = Zip();
				_numBytesToSkip = begin - len;
			}
			return false;
		}

		bool curLineIsEmpty = (begin == end);

		size_t cur = begin;
		begin = end + 2; // skip \r and \n

		if (curLineIsEmpty) {
			_state = ReadingRawData;

			size_t len = _data.length();
			if (len < begin) {
				_data = Zip();
				_numBytesToSkip = begin - len;
				return false;
			}

			_data.skipBefore(begin);
			return readRawData();
		}

		Zip curLine = _data.extractZip(cur, end);

		size_t pos = curLine.indexOf(0, ':');
		enforce0(pos != -1);

		Zip key = curLine.extractZip(0, pos);
		Zip value = curLine.extractZip(pos+2);

		if (key == HttpHeaders[ContentLength]) {
			//int size = (int)strtol(value.ptr, NULL, 10);
			//_rawDataToRead = size;

			//_listener->onContentsSize(size);
		}

		_listener->onHeader(key, value);
	}
}

bool HttpRequestParser::readRawData()
{
	if (_rawDataToRead == 0) {
		return true;
	}

	enforce0(false);

	return false;
}