#include "HttpRequest.h"
#include "HttpRequests.h"

#include "common/Buffer.hpp"

#include <stdio.h>

HttpRequest::HttpRequest(String host, String path, HttpRequestType requestType) :
	_path(path), _requestType(requestType)
{
	addHeader(HttpHeader(Host, host));

	if (requestType == Post) {
		addHeader(HttpHeader(ContentLength, String()));
		addHeader(HttpHeader(ContentType, String(33, "application/x-www-form-urlencoded")));
	}
}

void HttpRequest::addParam( HttpParam param )
{
	_params.put(param);
}

void HttpRequest::addParam( String key, String value )
{
	addParam(HttpParam(key, value));
}

void HttpRequest::setHeader( const HttpHeader& header )
{
	Array<HttpHeader> headers = this->headers();
	for (size_t i = 0; i < headers.length; ++i) {
		if (headers[i].key == header.key) {
			headers[i].value = header.value;
			return;
		}
	}

	addHeader(header);
}

void HttpRequest::addHeader( const HttpHeader& header )
{
	_headers.put(header);
}

Array<HttpParam> HttpRequest::params()
{
	return _params.data();
}

Array<HttpHeader> HttpRequest::headers()
{
	return _headers.data();
}

HttpRequestType HttpRequest::requestType()
{
	return _requestType;
}

String HttpRequest::host()
{
	enforce(headers()[0].key == HttpHeaders[Host], "Internal consistency error");
	return headers()[0].value;
}

void HttpRequest::host(String host)
{
	enforce(headers()[0].key == HttpHeaders[Host], "Internal consistency error");
	headers()[0].value = host;
}

String HttpRequest::path()
{
	return _path;
}

void HttpRequest::path(String path)
{
	_path = path;
}

Array<char> HttpRequest::compose( Buffer<char>& buffer )
{
	Array<HttpParam> params = this->params();
	for (size_t i = 0; i < params.length; ++i) {
		buffer.put('&');
		buffer.put(params[i].key);
		buffer.put('=');
		buffer.put(params[i].value);
	}

	size_t requestOffset = buffer.length();
	size_t paramsEnd = requestOffset;

	buffer.put(HttpRequests[_requestType]);
	buffer.put(' ');
	buffer.put(path());
	if (_requestType == Get) {
		if (requestOffset != 0) {
			buffer[0] = '?';
			buffer.put(buffer.slice(0, requestOffset));
		}

		paramsEnd = 1;
	}
	buffer.put(String(11, " HTTP/1.1\r\n"));

	Array<HttpHeader> headers = this->headers();
	for (size_t i = 0; i < headers.length; ++i) {
		String key = headers[i].key;
		buffer.put(key);
		buffer.put(String(2, ": "));

		if (key == HttpHeaders[ContentLength]) {
			char tmp[16];
			//int len = sprintf_s(tmp, sizeof(tmp), "%d", requestOffset);
			int len = sprintf(tmp, "%d", requestOffset);
			buffer.put(String(len, tmp));
		} else {
			buffer.put(headers[i].value);
		}

		buffer.put('\r');
		buffer.put('\n');
	}

	buffer.put('\r');
	buffer.put('\n');
	buffer.put(buffer.slice(1, paramsEnd));

	return buffer.slice(requestOffset, buffer.length());
}