#include "HttpHeaderType.h"
#include "HttpHeaders.h"

#include "common/Array.hpp"

String HttpHeadersArray[] = {
	String(4, "Host"),
	String(6, "Accept"),
	String(14, "Content-Length"),
	String(12, "Content-Type"),
	String(17, "Transfer-Encoding"),
	String(10, "Connection"),
	String(10, "User-Agent"),
	String(15, "Accept-Encoding"),
};

Array<String> HttpHeaders = Array<String>(HttpHeaderType_MAX, HttpHeadersArray);