#pragma once

#include "HttpHeader.h"
#include "HttpResponseParserListener.h"
#include "HttpStatus.h"

#include "Zip.h" // Appender

#include "common/Array.h"

class HttpResponse : /*public Event<HttpResponse>,*/ public HttpResponseParserListener
{
public:
	HttpResponse();
	~HttpResponse();

	void onStatus(HttpStatusCode statusCode, Zip description);
	void onHeader(Zip key, Zip value);
	void onContentsSize(size_t size);
	void onContents(Array<char> data);
	void onContentsEnd();

	const HttpStatus& httpStatus() const;
	Array<HttpHeader> headers() const;
	Array<char> contents() const;

private:
	HttpStatus _httpStatus;
	Buffer<HttpHeader> _headers;
	Buffer<char> _contents;
};