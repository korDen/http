#include "HttpRequests.h"

#include "common/String.h"
#include "common/Array.hpp"

String HttpRequestsArray[] = {
	String(3, "GET"),
	String(4, "POST"),
};

Array<String> HttpRequests = Array<String>(2, HttpRequestsArray);