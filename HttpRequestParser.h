#pragma once

#include "Zip.h"

#include "common/String.h"

struct HttpHeader;
struct HttpRequestParserListener;

class HttpRequestParser
{
public:
	HttpRequestParser(HttpRequestParserListener* listener);
	~HttpRequestParser();

	bool parse(Array<char> data);

private:
	enum ParsingState
	{
		ReadingRequest,
		ReadingHeaders,
		ReadingRawData,
		Complete,
	};

	bool readRequest();
	bool readHeaders();
	bool readRawData();

	ParsingState _state;

	Zip _data;

	size_t _numBytesToSkip;
	size_t _rawDataToRead;

	char* _storage;
	unsigned int _storageCapacity;

	HttpRequestParserListener* _listener;
};