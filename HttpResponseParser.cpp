#include "HttpResponseParser.h"
#include "HttpHeader.h"

#include "common/Memory.h"

HttpResponseParser::HttpResponseParser(HttpResponseParserListener* listener) : _state(ReadingResponse), _storage(nullptr), _numBytesToSkip(0), _chunked(false), _listener(listener), _rawDataToRead(0)
{
	// do nothing
}

HttpResponseParser::~HttpResponseParser()
{
	free(_storage);
}

bool HttpResponseParser::parse( Array<char> data )
{
	bool finished = false;

	if (_numBytesToSkip != 0) {
		size_t len = data.length;
		if (len > _numBytesToSkip) {
			data = data.slice(_numBytesToSkip);
			_numBytesToSkip = 0;
		} else {
			_numBytesToSkip -= len;
			if (_numBytesToSkip == 0 && _state == Complete) {
				goto done;
			}
		}
	}

	// Now append data to existing data:
	// _data = _data + data

	if (_data.cur.length != 0) {
		// merge prev and cur into prev
		unsigned int size = _data.length();
		if (_storageCapacity < size) {
			_storage = (char*)reallocMove(_storage, size, 0);
			_storageCapacity = size;
		}

		memcpy(_storage, _data.prev.ptr, _data.prev.length);
		memcpy(_storage + _data.prev.length, _data.cur.ptr, _data.cur.length);
		_data.prev = Array<char>(size, _storage);
	}
	
	_data.cur = data;

	_numBytesToSkip = 0;

	switch (_state) {
		case ReadingResponse:
			finished = readResponse();
			break;

		case ReadingHeaders:
			finished = readHeaders();
			break;

		case ReadingChunkedHeader:
			finished = readChunkedHeader();
			break;

		case ReadingChunkedData:
			finished = readChunkedData();
			break;

		case ReadingRawData:
			finished = readRawData();
			break;

		default:
			enforce0(false);
			break;
	}

done:
	if (finished) {
		_state = Complete;
		return true;
	}

	return false;
}

bool HttpResponseParser::readRawData()
{
//	enforce0(_rawDataToRead > 0);

	size_t read = _rawDataToRead;

	size_t dataLength = _data.length();
	if (dataLength < _rawDataToRead) {
		read = dataLength;
	}

	_data.commitTo(_listener, 0, read);
	_rawDataToRead -= read;

	return !_chunked && (_rawDataToRead == 0);
}

bool HttpResponseParser::readHeaders()
{
	size_t begin = 0;
	while (true) {
		size_t end = _data.indexOf(begin, '\r');
		if (end == -1) {
			size_t len = _data.length();
			if (len >= begin) {
				_data.skipBefore(begin);
			} else {
				_data = Zip();
				_numBytesToSkip = begin - len;
			}
			return false;
		}

		bool curLineIsEmpty = (begin == end);

		size_t cur = begin;
		begin = end + 2; // skip \r and \n

		if (curLineIsEmpty) {
			if (_chunked) {
				// end of headers -> chunked
				_state = ReadingChunkedHeader;
				_chunkedDataSize = 0;

				size_t len = _data.length();
				if (len < begin) {
					_data = Zip();
					_numBytesToSkip = begin - len;
					return false;
				}

				_data.skipBefore(begin);
				return readChunkedHeader();
			}

			_state = ReadingRawData;

			size_t len = _data.length();
			if (len < begin) {
				_data = Zip();
				_numBytesToSkip = begin - len;
				return false;
			}

			_data.skipBefore(begin);
			return readRawData();
		}

		Zip curLine = _data.extractZip(cur, end);

		size_t pos = curLine.indexOf(0, ':');
		enforce0(pos != -1);

		Zip key = curLine.extractZip(0, pos);
		Zip value = curLine.extractZip(pos + 2);

		if (key == HttpHeaders[ContentLength]) {
			size_t size = value.toInt();
			_listener->onContentsSize(size);
			_rawDataToRead = size;
		} else if (key == HttpHeaders[TransferEncoding]) {
			enforce0(value == String(7, "chunked"));
			_chunked = true;
		}

		_listener->onHeader(key, value);
	}
}

bool HttpResponseParser::readResponse()
{
	size_t pos = _data.indexOf(0, '\n');
	if (pos == -1) {
		return false;
	}

	size_t spacePos = _data.indexOf(0, ' ');
	Zip httpVersion = _data.extractZip(0, spacePos);

	size_t spacePos2 = _data.indexOf(spacePos + 1, ' ');
	Zip resultCode = _data.extractZip(spacePos + 1, spacePos2);

	Zip description = _data.extractZip(spacePos2 + 1, pos - 1);

	_listener->onStatus((HttpStatusCode)resultCode.toInt(), description);

	_data.skipBefore(pos + 1);

	_state = ReadingHeaders;

	return readHeaders();
}

bool HttpResponseParser::readChunkedHeader()
{
	enforce0(_state == ReadingChunkedHeader);

	// read size
	size_t size = _chunkedDataSize;

	size_t i = 0;
	size_t len = _data.length();

	while (true) {
		if (i == len) {
			_data = Zip();
			_chunkedDataSize = size;
			return false;
		}

		char c = _data[i];
		if (c == '\r') {
			break;
		}

		if (c >= 'a' && c <= 'f') {
			c -= 'a' - 10;
		} else if (c >= 'A' && c <= 'F') {
			c -= 'A' - 10;
		} else {
			enforce0(c >= '0' && c <= '9');
			c -= '0';
		}

		size = size * 16 + c;

		++i;
	}

	_data.skipBefore(i);

	if (size != 0) {
		_listener->onContentsSize(size);

		_state = ReadingChunkedData;
		_chunkedDataToRead = size;

		len = _data.length();
		if (len < 2) {
			_data = Zip();
			_numBytesToSkip = 2 - len;
			return false;
		}

		_data.skipBefore(2);
		return readChunkedData();
	}

	_state = Complete;

	len = _data.length();

	if (len < 4) {
		_data = Zip();
		_numBytesToSkip = 4 - len;
		return false;
	}

	return true;
}

bool HttpResponseParser::readChunkedData()
{
	size_t dataLength = _data.length();
	if (dataLength < _chunkedDataToRead) {
		_chunkedDataToRead -= dataLength;
		_data.commitTo(_listener, 0, dataLength);
		enforce0(_data.length() == 0);
		return false;
	}

	_data.commitTo(_listener, 0, _chunkedDataToRead);

	_state = ReadingChunkedHeader;
	_chunkedDataSize = 0;

	size_t len = _data.length();
	if (len < 2) {
		_data = Zip();
		_numBytesToSkip = 2 - len;
		return false;
	}

	_data.skipBefore(2);
	return readChunkedHeader();
}
